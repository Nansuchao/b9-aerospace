PART
{
    // --- general parameters ---
    name = B9_Engine_Jet_Pod_Small
    module = Part
    author = bac9-flcl (textures), Taverius (model)

    // --- asset parameters ---
    mesh = model.mu
    scale = 1.0
    rescaleFactor = 1

    // --- node definitions ---
    node_stack_top = 0.0, 0.0, 0.5, 0.0, 0.0, 1.0, 0
    node_attach = 0.0, 0.12, 0.5, 0.0, 0.0, -1.0

    // --- editor parameters ---
    TechRequired = aerodynamicSystems
    entryCost = 2400
    cost = 400
    category = Engine
    subcategory = 0
    title = TFE731 Turbofan Engine
    manufacturer = Tetragon Projects
    description = Small and reliable high-bypass subsonic turbofan. Suitable for wide variety of tasks, from powering crew transports to small cargo airplanes. Use small structural pylons for attachment. Max Design Speed: Mach 0.95. Effective Intake Area: 0.00512

    // attachment rules: stack, srfAttach, allowStack, allowSrfAttach, allowCollision
    attachRules = 1,1,0,1,0

    // --- standard part parameters ---
    mass = 0.49
    dragModelType = default
    maximum_drag = 0.2
    minimum_drag = 0.2
    angularDrag = 2
    crashTolerance = 7
    breakingForce = 2000
    breakingTorque = 2000
    maxTemp = 2000 // = 3600
	emissiveConstant = 0.8
	bulkheadProfiles = size1, srf

    EFFECTS
    {
        power
        {
            AUDIO
            {
                channel = Ship
                clip = sound_jet_deep
                volume = 0   0    0     25
                volume = 0.1 1.12 0.145 0.145
                volume = 1.0 1.25 0.145 0
                pitch = 0 0.3
                pitch = 1 1
                loop = true
            }
        }
        running
        {
            MODEL_MULTI_PARTICLE_PERSIST
            {
                name = b9_jet_tfe_smoke
                modelName = B9_Aerospace/FX/HR/smokejet
                transformName = thrust_transform
                speed = 0.0 0.8
                speed = 1.0 1
                size = 0.5

                emission
                {
                    power = 0.07 0 0 0
                    power = 0.2 0.4 2.91 2.91
                    power = 1.0 1.1 0 0
                    density = 0.001 0 0 0
                    density = 0.002 1 0 0
                }

                energy
                {
                    power = 0 0.2
                    power = 1 3.2
                    externaltemp = -10 1
                    externaltemp = 1 0
                    mach = 0 1
                    mach = 10 10
                }

                offset
                {
                    mach = 0 1
                    mach = 6 10
                }

                logGrow
                {
                    density = 0 2
                    density = 1 0.9
                    density = 10 0
                }
            }

            AUDIO
            {
                channel = Ship
                clip = sound_jet_low
                volume = 0   0    0    25
                volume = 0.1 1.12 0.22 0.22
                volume = 1   1.32 0.22 0
                pitch = 0 0.3
                pitch = 1 1
                loop = true
            }
        }
        engage
        {
            AUDIO
            {
                channel = Ship
                clip = sound_vent_medium
                volume = 1
                pitch = 2
                loop = false
            }
        }
        disengage
        {
            AUDIO
            {
                channel = Ship
                clip = sound_vent_soft
                volume = 1
                pitch = 2
                loop = false
            }
        }
        flameout
        {
            PREFAB_PARTICLE
            {
                prefabName = fx_exhaustSparks_flameout_2
                transformName = thrust_transform
                oneShot = true
                localOffset = 0, 0, 0.9
            }

            AUDIO
            {
                channel = Ship
                clip = sound_explosion_low
                volume = 1
                pitch = 2
                loop = false
            }
        }
    }
	
	// Data based on TFE731-60
    MODULE
    {
        name = ModuleEnginesFX
        engineID = Jet
        thrustVectorTransformName = thrust_transform
        exhaustDamage = False
        ignitionThreshold = 0.1
        minThrust = 0
        maxThrust = 22.2
        heatProduction = 60 // Multiplied by mass in global config
		useEngineResponseTime = True
        engineAccelerationSpeed = 0.35
        engineDecelerationSpeed = 0.55
        useVelocityCurve = False
		EngineType = Turbine
		
		PROPELLANT
        {
            name = LiquidFuel
			resourceFlowMode = STAGE_PRIORITY_FLOW
            ratio = 1
            DrawGauge = True
        }
        PROPELLANT
        {
            name = IntakeAir
            ignoreForIsp = True
			ratio = 90
        }
		atmosphereCurve
		{
			key = 0 8900 0 0
		}
        atmChangeFlow = True
		useVelCurve = True
		useAtmCurve = False
		machLimit = 0.75
		machHeatMult = 200.0
		velCurve
		{
			// Based on Fnet data from AJE
			key = 0.0	1.0				-0.678569255043		-0.678569255043
			key = 0.61	0.822995015409	0.000142167637622	0.000142167637622
			key = 1.0	0.862913302078	0.174502868031		0.174502868031
			key = 1.19	0.880316601476	0.0951547076612		0.0951547076612
			key = 2.15	0.0				-2.15782652796		-2.15782652796
		}
    }

    MODULE
    {
        name = ModuleAnimateHeat
        ThermalAnim = jet_pod_small_heat
    }

    MODULE
    {
        name = ModuleAlternator
        RESOURCE
        {
            name = ElectricCharge
            rate = 4.0
        }
    }

    RESOURCE
    {
        name = ElectricCharge
        amount = 0
        maxAmount = 0
        isTweakable = false
        hideFlow = true
    }

    MODULE
    {
        name = ModuleResourceIntake
        resourceName = IntakeAir
        checkForOxygen = true
        area = 0.00512
        intakeTransformName = intake
    }

    RESOURCE
    {
        name = IntakeAir
        amount = 1.0 // dummy value
        maxAmount = 1.0
    }

    MODULE
    {
        name = FSplanePropellerSpinner
        propellerName = fan_holder
        rotationSpeed = -31 // -600
        thrustRPM = -467 // -4200
        spinUpTime = 3.5
        useRotorDiscSwap = 0
        //rotorDiscName = fan_disc
        //blade1 = fan_blades
        //rotorDiscFadeInStart = 0.6
        //rotorDiscSpeed = -30
    }

    MODULE
    {
        name = ModuleTestSubject

        // nowhere: 0, srf: 1, ocean: 2, atmo: 4, space: 8
        environments = 7

        useStaging = True
        useEvent = True
    }
	
	MODULE
	{
		name = ModuleSurfaceFX
		thrustProviderModuleIndex = 0
		fxMax = 0.3
		maxDistance = 15
		falloff = 2
		thrustTransformName = thrust_transform
	}
}

@PART[B9_Engine_Jet_Pod_Small]:NEEDS[!FerramAerospaceResearch]
{
	@MODULE[ModuleEnginesFX]
	{
		!velCurve {}
		velCurve
		{
			key = 0.0	1.0		 0.0	-0.125
			key = 0.35	0.95	 0.0	 0.0
			key = 0.85	1.095	 0.5	 0.5
			key = 1.5	0.0		-5.0	 0.0
		}
		@useAtmCurve = True
		atmCurve
		{
			key = 0 0 0 0
			key = 0.1 0.1 1.276916 1.276916
			key = 0.297 0.35 1.304143 1.304143
			key = 0.538 0.59 0.8485174 0.8485174
			key = 1 1 0.8554117 0
		}
	}
}
