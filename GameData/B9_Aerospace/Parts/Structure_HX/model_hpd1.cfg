PART
{
    // --- general parameters ---
    name = B9_Structure_HX_HPD1
    module = Part
    author = bac9

    // --- asset parameters ---
    MODEL
    {
        model = B9_Aerospace/Parts/Structure_HX/model_hpd1
    }
    scale = 1
    rescaleFactor = 1

    // --- node definitions ---
    // definition format is Position X, Position Y, Position Z, Up X, Up Y, Up Z
    node_stack_top1 = 0.0, 2, 0.0, 0.0, 1.0, 0.0, 6

    // --- editor parameters ---
    TechRequired = experimentalRocketry
    entryCost = 44750
    cost = 340500 // 227681 * 1.5
    category = Propulsion
    subcategory = 0
    title = HX-HPD Heavy Propulsion Device
    manufacturer = Tetragon Projects
    description = This enormous dual-mode engine was engineered for missions with extremely heavy payloads such as delivering HX modules to orbit or powering interplanetary transfers of structures exceeding thousands of tonnes. It combines the performance of a traditional closed-cycle rocket engine and that of a nuclear-powered LV-N engine in one propulsion assembly. The tech behind it is unclear and Tetragon Projects has declined to respond to inquiries, but it appears to use electromagnetic fields to direct and accelerate exhaust gases. The primary mode uses relatively small amounts of power supplied by dedicated gas generators, while secondary mode draws enormous amounts of power, which must be supplied externally - likely by the HX-URC Reactor, designed for this purpose. Note that the secondary mode is not designed to be run at full throttle continuously, and the HX-URC18 has been designed appropriately. Attempts to power it with solar panel arrays, though technically possible, have so far been unsuccessful.

    // attachment rules: stack, srfAttach, allowStack, allowSrfAttach, allowCollision
    attachRules = 1,0,1,1,0

    // --- standard part parameters ---
    mass = 52
    dragModelType = default
    maximum_drag = 0.2
    minimum_drag = 0.15
    angularDrag = 2
    crashTolerance = 10
    maxTemp = 2000 // = 3400
    fuelCrossFeed = True
    breakingForce = 1388800
    breakingTorque = 1388800
    bulkheadProfiles = hx
	
    EFFECTS
    {
        power_closed
        {
            MODEL_MULTI_PARTICLE_PERSIST
            {
                name = b9_hpd_flame_closed
                modelName = B9_Aerospace/FX/HR/nasa4engine
                transformName = nozzle_transform
                localPosition = 0, 0, 2
                speed = 0 1.65
                speed = 1 1.7
                logGrow
                {
                    density = 1 -0.1
                    density = 0.4 1
                    density = 0.05 10
                    density = 0.005 30
                }
                linGrow
                {
                    density = 0.05 0
                    density = 0.005 0.4
                    density = 0 2
                }
                offset
                {
                    power = -0.2
                    density = 1 0
                    density = 0.1 1.5
                    density = 0.05 1.5
                    density = 0.03 2.3
                    density = 0.005 2.7
                    density = 0 2.65
                }
                emission
                {
                    power = 0.05 0
                    power = 0.06 0.1
                    power = 0.75 0.22
                    power = 1 0.3
                    density = 1 0.7
                    density = 0.05 0.5
                    density = 0.005 0.45
                    density = 0 0.4
                }
                energy
                {
                    power = 0 4.8
                    power = 1 18
                    density = 1 0.3
                    density = 0.05 0.2
                    density = 0.005 0.19
                    density = 0.002 0.18
                    density = 0 0.18
                }
                size
                {
                    power = 7
                    density = 0.05 0.7
                    density = 0.005 0.56
                    density = 0 0.5
                }
            }
            AUDIO
            {
                channel = Ship
                clip = sound_rocket_hard
                volume = 0.0 0.0
                volume = 1.0 1.0
                pitch = 0.0 0.2
                pitch = 1.0 1.0
                loop = true
            }
        }
        power_plasma
        {
            MODEL_MULTI_PARTICLE_PERSIST
            {
                name = b9_hpd_flame_plasma
                modelName = B9_Aerospace/FX/HR/flamenuke
                transformName = nozzle_transform
                fixedScale = 4
                localPosition = 0, 0, 0.2
                speed = 0 1.98
                speed = 1 1.22
                energy = 0 1.32
                energy = 1 3.96
                logGrow
                {
                    density = 1 -0.25
                    density = 0.005 15
                }
                linGrow
                {
                    density = 0.005 0
                    density = 0 5
                }
                offset
                {
                    density = 1 0
                    density = 0.5 0
                    density = 0.005000001 0
                    density = 0.005 0
                    density = 0 0
                }
                size
                {
                    density = 1 1
                    density = 0.5 0.7
                    density = 0.005000001 0.56
                    density = 0.005 0.56
                    density = 0 0.47
                }
                emission
                {
                    power = 0 0
                    power = 0.05 0.99
                    power = 0.75 1.21
                    power = 1 1.25
                    density = 1 1
                    density = 0.005000001 0.7
                    density = 0.005 0.7
                    density = 0 0.5
                }
            }
            AUDIO
            {
                channel = Ship
                clip = sound_rocket_hard
                volume = 0.0 0.0
                volume = 1.0 1.0
                pitch = 0.0 0.2
                pitch = 1.0 1.0
                loop = true
            }
        }
        running
        {
            MODEL_MULTI_PARTICLE_PERSIST
            {
                name = b9_hpd_smoke
                modelName = B9_Aerospace/FX/HR/smokejet
                transformName = nozzle_transform
                fixedScale = 3
                speed = 0.0 0.8
                speed = 1.0 1.0

                emission
                {
                    power = 0.07 0 0 0
                    power = 0.2 0.4 2.91 2.91
                    power = 1.0 1.1 0 0
                    density = 0.001 0 0 0
                    density = 0.002 1 0 0
                }

                energy
                {
                    power = 0.0 0.2
                    power = 1.0 3.2
                    externaltemp = -10 1
                    externaltemp = 1 0
                    mach = 0 1
                    mach = 10 10
                }

                offset
                {
                    mach = 0 1
                    mach = 6 10
                }

                logGrow
                {
                    density = 0 2
                    density = 1 0.9
                    density = 10 0
                }
            }
        }
        engage
        {
            AUDIO
            {
                channel = Ship
                clip = sound_vent_medium
                volume = 1.0
                pitch = 2.0
                loop = false
            }
        }
        disengage
        {
            AUDIO
            {
                channel = Ship
                clip = sound_vent_soft
                volume = 1.0
                pitch = 2.0
                loop = false
            }
        }
        flameout
        {
            AUDIO
            {
                channel = Ship
                clip = sound_explosion_low
                volume = 1.0
                pitch = 2.0
                loop = false
            }
        }
    }

    MODULE
    {
        name = MultiModeEngine
        primaryEngineID = ClosedCycle
        secondaryEngineID = HybridPlasma
    }

    MODULE
    {
        name = ModuleEnginesFX
        engineID = ClosedCycle
        powerEffectName = power_closed
        thrustVectorTransformName = nozzle_transform
        exhaustDamage = true
        ignitionThreshold = 0.1
        minThrust = 0
        maxThrust = 19080 // 21200 * 0.9
        heatProduction = 390
        useEngineResponseTime = True
        engineAccelerationSpeed = 1.60
        engineDecelerationSpeed = 2
        PROPELLANT
        {
            name = LiquidFuel
            ratio = 0.9
            DrawGauge = True
        }
        PROPELLANT
        {
            name = Oxidizer
            ratio = 1.1
        }
        atmosphereCurve
        {
            key = 0 323 // 308 * 1.05
            key = 1 281 // 268 * 1.05
        }
    }

    MODULE
    {
        name = ModuleEnginesFX
        engineID = HybridPlasma
        powerEffectName = power_plasma
        thrustVectorTransformName = nozzle_transform
        exhaustDamage = true
        ignitionThreshold = 0.1
        minThrust = 0
        maxThrust = 4280 // 4761 * 0.9
        heatProduction = 500
        useEngineResponseTime = True
        engineAccelerationSpeed = 1.60
        engineDecelerationSpeed = 2
        PROPELLANT
        {
            name = ElectricCharge
            ratio = 16.5
        }
        PROPELLANT
        {
            name = LiquidFuel
            ratio = 0.9
            DrawGauge = True
        }
        PROPELLANT
        {
            name = Oxidizer
            ratio = 1.1
        }
        atmosphereCurve
        {
            key = 0 840 // 800 * 1.05
            key = 1 230 // 220 * 1.05
        }
    }

    MODULE
    {
        name = KM_Gimbal
        gimbalTransformName = nozzle_transform
        pitchGimbalRange = 5
        yawGimbalRange = 5
        enableRoll = true
        enableSmoothGimbal = true
        responseSpeed = 80
    }

    MODULE
    {
        name = ModuleAnimateHeat
        ThermalAnim = hx_hpd2_heat
    }

    MODULE
    {
        name = ModuleTestSubject

        // nowhere: 0, srf: 1, ocean: 2, atmo: 4, space: 8
        environments = 15

        useStaging = True
        useEvent = True
    }
}
